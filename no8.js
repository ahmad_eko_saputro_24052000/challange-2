const dataPenjualanNovel = [
    {
      idProduct: 'BOOK002421',
      namaProduk: 'Pulang - Pergi',
      penulis: 'Tere Liye',
      hargaBeli: 60000,
      hargaJual: 86000,
      totalTerjual: 150,
      sisaStok: 17,
    },
    {
      idProduct: 'BOOK002351',
      namaProduk: 'Selamat Tinggal',
      penulis: 'Tere Liye',
      hargaBeli: 75000,
      hargaJual: 103000,
      totalTerjual: 171,
      sisaStok: 20,
    },
    {
      idProduct: 'BOOK002941',
      namaProduk: 'Garis Waktu',
      penulis: 'Fiersa Besari',
      hargaBeli: 67000,
      hargaJual: 99000,
      totalTerjual: 213,
      sisaStok: 5,
    },
    {
      idProduct: 'BOOK002941',
      namaProduk: 'Laskar Pelangi',
      penulis: 'Andrea Hirata',
      hargaBeli: 55000,
      hargaJual: 68000,
      totalTerjual: 20,
      sisaStok: 56,
    },
  ]
  
    
  let totalBeli = 0
  let totalHarga = 0
  let totalAwal = 0
  let totalTer = 0
  let totalSel = 0
  
  dataPenjualanNovel.forEach(function(elem){
    totalHarga += elem.hargaJual;
    totalBeli += elem.hargaBeli;
    totalAwal = totalHarga - totalBeli;
     totalTer += elem.totalTerjual;
    totalSel = totalAwal * totalTer;
  
    return totalSel;
  });
    {

      const format = totalSel.toString().split('').reverse().join('');
      const convert = format.match(/\d{1,3}/g);
      const rupiah = 'total keuntungan: Rp. '+ convert.join('.').split('').reverse().join('')
      
      
      console.log(rupiah);
    }


  let totalBeli1 = 0
  let totalStok = 0
  let totalAwal1 = 0
  let totalTer1 = 0
  let totalSel1 = 0
  
  dataPenjualanNovel.forEach(function(elem){
    totalStok += elem.sisaStok;
    totalTer1 += elem.totalTerjual;
    totalAwal1 = totalStok + totalTer1;
    totalBeli1 += elem.hargaBeli;
    totalSel1 = totalAwal1 * totalBeli1;
  
    return totalSel;
  });
    {

      const format = totalSel1.toString().split('').reverse().join('');
      const convert = format.match(/\d{1,3}/g);
      const rupiah = 'total modal: Rp. '+ convert.join('.').split('').reverse().join('')
      
      
      console.log(rupiah);
    }




   const getInfoPenjualan = (listBuku) => {
return listBuku.reduce((prev, cur) => cur.totalTerjual > (prev.totalTerjual || 0 )
 ? cur : 'Produk Buku Terlaris: ' + prev.namaProduk + '\n' + 'Penulis Terlaris: '+ prev.penulis
    );
  
  }
console.log(getInfoPenjualan(dataPenjualanNovel));